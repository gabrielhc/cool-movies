import { css } from '@emotion/react';
import { Container, Typography } from '@mui/material';
import type { NextPage } from 'next';

import Illustration from '../components/Illustrations/Error404';
import Layout from '../components/Layout';
import theme from '../theme/theme';

const Error404: NextPage = () => {
  return (
    <Layout>
      <Container
        component="main"
        maxWidth={'md'}
        css={styles.main}
        color="primary"
      >
        <Typography component="h1" variant="h2" css={styles.title}>
          {'Oops...'}
        </Typography>
        <Typography component="h2" variant="h3">
          {'Page not found'}
        </Typography>
        <Illustration></Illustration>
      </Container>
    </Layout>
  );
};

const styles = {
  main: css({
    padding: theme.spacing(6),
    textAlign: 'center',
  }),
  title: css({
    fontWeight: '700',
    paddingBottom: theme.spacing(3),
  }),
};

export default Error404;
