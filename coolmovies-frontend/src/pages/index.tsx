import { css } from '@emotion/react';
import { Container, Typography } from '@mui/material';
import type { NextPage } from 'next';

import Layout from '../components/Layout';
import MovieList from '../containers/MovieList';
import theme from '../theme/theme';

const Home: NextPage = () => {
  return (
    <Layout>
      <Container maxWidth={'lg'} component="main" css={styles.main}>
        <Typography
          component="h1"
          variant="h2"
          color="primary"
          css={styles.title}
        >
          {'Cool Movie List'}
        </Typography>
        <Typography component="h2" variant="h4" css={styles.subtitle}>
          {'Check the comments and reviews of the following movies'}
        </Typography>
        <MovieList />
      </Container>
    </Layout>
  );
};

const styles = {
  main: css({
    textAlign: 'center',
  }),
  title: css({
    paddingTop: theme.spacing(4),
    fontWeight: '700',
  }),
  subtitle: css({
    paddingBottom: theme.spacing(4),
  }),
};

export default Home;
