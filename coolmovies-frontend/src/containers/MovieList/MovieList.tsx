import React, { useEffect } from 'react';
import { Box } from '@mui/system';

import Empty from '../../components/Empty';
import MovieGrid from '../../components/MovieGrid';
import TryAgain from '../../components/TryAgain';
import { moviesActions, useAppDispatch, useAppSelector } from '../../redux';

const MovieList = () => {
  const dispatch = useAppDispatch();
  const movieState = useAppSelector((state) => state.movie);

  const isLoading = movieState.isLoading;
  const hasError = !!movieState.error;
  const movies = Object.values(movieState.movies || {});

  useEffect(() => {
    dispatch(moviesActions.fetchMovies());
  }, [dispatch]);

  if (hasError) {
    return <TryAgain action={moviesActions.fetchMovies} />;
  }

  if (!movies && !isLoading) {
    return <Empty />;
  }

  return (
    <Box textAlign="center">
      <MovieGrid
        movies={movies}
        isLoading={isLoading}
        loadItems={2}
      ></MovieGrid>
    </Box>
  );
};

export default MovieList;
