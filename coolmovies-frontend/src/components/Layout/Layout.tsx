import { css } from '@emotion/react';

import Footer from '../Footer';
import Header from '../Header';

type MainContent = {
  children: JSX.Element;
};

const Layout = ({ children }: MainContent) => {
  return (
    <div css={styles.root}>
      <Header />
      {children}
      <Footer />
    </div>
  );
};

const styles = {
  root: css({
    minHeight: '100vh',
    display: 'grid',
    gridTemplateRows: 'auto 1fr auto',
  }),
};

export default Layout;
