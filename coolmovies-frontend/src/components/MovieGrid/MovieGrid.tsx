import React from 'react';
import { css } from '@emotion/react';
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Skeleton,
  Typography,
} from '@mui/material';
import Link from 'next/link';

import theme from '../../theme/theme';
import { Movie } from '../../types/Movie/Movie';

interface MovieGridProps {
  movies: Movie[];
  isLoading: boolean;
  loadItems?: number;
}

const MovieGrid = ({ movies, isLoading, loadItems = 2 }: MovieGridProps) => {
  const data = isLoading ? Array.from(new Array(loadItems)) : movies;

  return (
    <div css={styles.root}>
      {data.map((movie: Movie, index) => {
        return (
          <Card key={isLoading ? index : movie.id}>
            <CardActionArea>
              {isLoading ? (
                <Skeleton
                  animation="wave"
                  variant="rectangular"
                  height={200}
                  width={300}
                />
              ) : (
                <CardMedia
                  component="img"
                  src={movie.imgUrl}
                  alt={`${movie.title} poster`}
                  height={200}
                  width={200}
                />
              )}

              <CardContent>
                <Typography gutterBottom variant="h5">
                  {isLoading ? <Skeleton /> : movie.title}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              {isLoading ? (
                <>
                  <Skeleton height={30} width={90}></Skeleton>
                  <Skeleton height={30} width={100}></Skeleton>
                </>
              ) : (
                <>
                  <Link href={`/reviews/${movie.id}`} passHref>
                    <Button size="small">Reviews</Button>
                  </Link>
                  <Link href={`/comments/${movie.id}`} passHref>
                    <Button size="small">Comments</Button>
                  </Link>
                </>
              )}
            </CardActions>
          </Card>
        );
      })}
    </div>
  );
};

const styles = {
  root: css({
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    gap: '1rem',
    padding: theme.spacing(2),
  }),
};

export default MovieGrid;
