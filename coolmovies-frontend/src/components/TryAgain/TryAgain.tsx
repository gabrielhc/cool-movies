import ReplayIcon from '@mui/icons-material/Replay';
import { Box, Button, Typography } from '@mui/material';
import { ActionCreatorWithoutPayload } from '@reduxjs/toolkit';

import { useAppDispatch } from '../../redux';

interface TryAgainProp {
  errorMessage?: string;
  action: ActionCreatorWithoutPayload;
}

const TryAgain = ({
  errorMessage = 'We were unable to fetch your movies',
  action,
}: TryAgainProp) => {
  const dispatch = useAppDispatch();

  const handleClick = () => {
    dispatch(action());
  };

  return (
    <Box sx={{ textAlign: 'center' }}>
      <Typography variant="h5">{errorMessage}</Typography>
      <Button
        onClick={handleClick}
        variant="outlined"
        color="error"
        endIcon={<ReplayIcon />}
      >
        Try again
      </Button>
    </Box>
  );
};

export default TryAgain;
