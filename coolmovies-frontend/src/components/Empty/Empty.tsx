import { Typography } from '@mui/material';

import EmptyIllustration from '../Illustrations/Empty';

const Empty = () => (
  <div>
    <Typography
      variant={'h3'}
    >{`Strange there is no movie here...`}</Typography>
    <EmptyIllustration></EmptyIllustration>
    <Typography
      variant={'caption'}
    >{`"And if thou gaze long into an abyss, the abyss will also gaze into thee"`}</Typography>
  </div>
);

export default Empty;
