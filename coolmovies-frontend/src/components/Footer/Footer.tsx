import { Typography } from '@mui/material';

import { styles } from './style';

const Footer = () => (
  <footer css={styles.footer}>
    <Typography>{'Made with 💚 by Gabriel Claudino'}</Typography>
  </footer>
);

export default Footer;
