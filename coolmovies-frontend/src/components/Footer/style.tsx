import { css } from '@emotion/react';
import { grey } from '@mui/material/colors';

export const styles = {
  footer: css({
    background: grey[400],
    textAlign: 'center',
    padding: '0.5rem',
  }),
};
