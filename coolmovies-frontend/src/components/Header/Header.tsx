import {
  AppBar,
  Avatar,
  Box,
  Button,
  IconButton,
  Link,
  Toolbar,
} from '@mui/material';
import { deepPurple } from '@mui/material/colors';
import Image from 'next/image';
import NextLink from 'next/link';

import { styles } from './style';

const Header = () => (
  <AppBar position="sticky" css={styles.appBar} elevation={2}>
    <Toolbar css={styles.toolbar}>
      <NextLink href="/" passHref>
        <Link href="/" sx={{ display: 'flex' }}>
          <Image
            src="/logo.svg"
            alt="Logo"
            loading="lazy"
            height="39"
            width="182"
          />
        </Link>
      </NextLink>
      <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
        <NextLink href="/" passHref>
          <Button
            variant="text"
            css={styles.button}
            size="large"
            disableElevation
          >
            Movies
          </Button>
        </NextLink>

        <NextLink href="/reviews" passHref>
          <Button
            variant="text"
            css={styles.button}
            size="large"
            disableElevation
          >
            Reviews
          </Button>
        </NextLink>
      </Box>

      <IconButton sx={{ p: 0 }}>
        <Avatar sx={{ bgcolor: deepPurple[500] }}>UN</Avatar>
      </IconButton>
    </Toolbar>
  </AppBar>
);

export default Header;
