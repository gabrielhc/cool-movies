import { css } from '@emotion/react';
import { grey } from '@mui/material/colors';

import theme from '../../theme/theme';

export const styles = {
  appBar: css({
    backgroundColor: grey['100'],
  }),
  toolbar: css({
    display: 'flex',
    justifyContent: 'space-between',
  }),
  button: css({
    padding: '1rem',
    color: grey['900'],
    '&:hover': {
      color: theme.palette.primary.main,
    },
  }),
};
