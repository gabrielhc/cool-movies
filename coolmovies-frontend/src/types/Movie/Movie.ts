export interface Movie {
  id: string;
  imgUrl: string;
  title: string;
}

export interface AllMoviesQuery {
  data: {
    allMovies: {
      nodes: Movie[];
    };
  };
}
