import { gql } from '@apollo/client';
import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

import { AllMoviesQuery } from '../../../types/Movie/Movie';
import { RootState } from '../../store';
import { EpicDependencies } from '../../types';

import { actions, SliceAction } from './slice';

export const asyncGetMovieEpic: Epic = (
  action$: Observable<SliceAction['fetchMovies']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchMovies.match),
    switchMap(async () => {
      try {
        const { data }: AllMoviesQuery = await client.query({
          query: allMoviesQuery,
        });

        const moviesArr = data.allMovies.nodes || [];

        const movies = moviesArr.reduce(
          (obj, item) => ({ ...obj, [item.id]: item }),
          {}
        );

        return actions.loaded({ movies });
      } catch (error) {
        const reportError =
          error instanceof Error ? error : new Error('Could not load movies');
        return actions.loadError({ error: reportError });
      }
    })
  );

const allMoviesQuery = gql`
  query AllMovies {
    allMovies {
      nodes {
        id
        imgUrl
        title
      }
    }
  }
`;
