import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Movie } from '../../../types/Movie/Movie';

interface MovieState {
  movies?: { [id: string]: Movie };
  isLoading: boolean;
  error?: Error;
}

const initialState: MovieState = {
  movies: undefined,
  isLoading: false,
  error: undefined,
};

export const slice = createSlice({
  initialState,
  name: 'movie',
  reducers: {
    fetchMovies: (state) => {
      state.movies = undefined;
      state.error = undefined;
      state.isLoading = true;
    },
    clearData: (state) => {
      state.movies = undefined;
      state.isLoading = false;
      state.error = undefined;
    },
    loaded: (
      state,
      { payload }: PayloadAction<{ movies: { [id: string]: Movie } }>
    ) => {
      state.isLoading = false;
      state.movies = payload.movies;
    },
    loadError: (state, { payload }: PayloadAction<{ error: Error }>) => {
      state.isLoading = false;
      state.error = payload.error;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
