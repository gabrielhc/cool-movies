import { combineEpics } from 'redux-observable';

export { actions as moviesActions } from './slice';
export { default as moviesReducer } from './slice';
import { asyncGetMovieEpic } from './epics';

export const movieEpics = combineEpics(asyncGetMovieEpic);
